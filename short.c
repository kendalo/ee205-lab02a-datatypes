///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file short.c
/// @version 1.0
///
/// Print the characteristics of the "short", "signed short" and "unsigned short" datatypes.
///
/// @author @Kendal Oya <@kendalo@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   @22 Jan 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <limits.h>

#include "datatypes.h"
#include "short.h"


///////////////////////////////////////////////////////////////////////////////
/// short

/// Print the characteristics of the "short" datatype
void doShort() {
   printf(TABLE_FORMAT_SHORT, "short", sizeof(short)*8, sizeof(short), SHRT_MIN, SHRT_MAX);
}


/// Print the overflow/underflow characteristics of the "short" datatype
void flowShort() {
   short overflow = SHRT_MAX;
   printf("short overflow:  %hd + 1 ", overflow++);
   printf("becomes %hd\n", overflow);

   short underflow = SHRT_MIN;
   printf("short underflow:  %hd - 1 ", underflow--);
   printf("becomes %hd\n", underflow);
}


///////////////////////////////////////////////////////////////////////////////
/// unsigned short

/// Print the characteristics of the "unsigned short" datatype
void doUnsignedShort() {
printf(TABLE_FORMAT_SHORT, "unsigned short", 16, 2, 0, USHRT_MAX);}

/// Print the overflow/underflow characteristics of the "unsigned short" datatype
void flowUnsignedShort() {
   unsigned short overflow = USHRT_MAX;
   printf("unsigned short overflow:  %hu + 1 ", overflow++);
   printf("becomes %hu\n", overflow);

   unsigned short underflow = 0;
   printf("unsigned short underflow:  %hu - 1 ", underflow--);
   printf("becomes %hu\n", underflow);
}

